import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import 'primereact/resources/themes/nova-light/theme.css';
import '@fortawesome/fontawesome-free/js/all.js';

ReactDOM.render(<App />, document.getElementById('root'));
