import React from 'react';
import {useTracked} from './Context'
import Iframe from 'react-iframe'

import {faucets} from './Context'

export const Faucet = () => {
    const [state, dispatch] = useTracked();
    const tmpFaucet = faucets[state.faucetIdx];
    const faucetURL = tmpFaucet.url + "faucet";
    const faucetColor = tmpFaucet.color;

    return (
        <div className="faucet-content" style={{background: faucetColor}}>
            <Iframe url={faucetURL} width="100%"  height="100%" id="iframe" className="FaucetFrame"
                    display="initial"
                    sandbox="allow-forms allow-pointer-lock allow-same-origin allow-scripts"
                    position="relative"/>
        </div>
    );
};