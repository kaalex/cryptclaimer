
import React from "react";
import {Switch, Route, Link} from "react-router-dom";
import {BrowserRouter as Router} from "react-router-dom";
import {useTracked} from './Context'

import {Info} from "./Info";
import Referral from "./Referral";
import Coins from "./Coins";
import CoinPot from "./CoinPot";
import {Faucet} from "./Faucet";
import {Sidebar} from "primereact/sidebar";


export const Routes = () => {
    const [state, dispatch] = useTracked();
    return (
        <Router>
            <div className="nav-menu">
                <Sidebar className="p-sidebar-sm" fullScreen={false} visible={state.menuVisible} onHide={() => dispatch({type: 'flipNavigation'})}>
                    <div>
                        <img className="logo" src={'android-chrome-192x192.png'} alt="Logo CryptoClaimer" />
                    </div>
                    <nav>
                        <ul className="nav-menu">
                            <li className="link">
                                <Link to="/start" onClick={() => dispatch({type: 'flipNavigation'})} ><i className="fa fa-fw fa-shoe-prints"/> Get Started</Link>
                            </li>
                            <li className="link">
                                <Link to="/faucet" onClick={() => dispatch({type: 'flipNavigation'})} ><i className="fa fa-fw fa-faucet"/> Faucet</Link>
                            </li>
                            <li className="link">
                                <Link to="/coins" onClick={() => dispatch({type: 'flipNavigation'})}><i className="fa fa-fw fa-coins"/> Coins</Link>
                            </li>
                            <li className="link">
                                <a href="https://coinmarketcap.com/" target="_blank" rel="noopener noreferrer"
                                   onClick={() => dispatch({type: 'flipNavigation'})} ><i className="fa fa-fw fa-external-link-alt"/> CoinMarketCap</a>
                            </li>
                            <li className="link">
                                <a href="https://coinpot.co/dashboard" target="_blank" rel="noopener noreferrer"
                                   onClick={() => dispatch({type: 'flipNavigation'})} ><i className="fa fa-fw fa-external-link-alt"/> CoinPot</a>
                            </li>
                            <li className="link">
                                <Link to="/info" onClick={() => dispatch({type: 'flipNavigation'})}><i className="fa fa-fw fa-info"/> Info</Link>
                            </li>
                        </ul>
                    </nav>
                </Sidebar>
            </div>
            <Switch>
                <Route path="/info" component={Info} />
                <Route path="/coins" component={Coins} />
                <Route path="/coinpot" component={CoinPot} />
                <Route path="/faucet" component={Faucet} />
                <Route path="/start" component={Referral} />
                <Route path="/" component={Referral} />
            </Switch>
        </Router>
    )
};