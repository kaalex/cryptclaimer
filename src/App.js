import React from 'react';

import './App.scss';
import {prevFaucet, nextFaucet, faucets} from "./Context";
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import {Toolbar} from 'primereact/toolbar';
import {Button} from 'primereact/button';
import {Provider, useTracked, initialState, reducer} from './Context'
import {Routes} from "./Routes";
import {BrowserRouter as Router} from "react-router-dom";
import {Helmet} from "react-helmet";

const cmcWidget = (state) => {
    const currencyID = faucets[state.faucetIdx].currencyID;
    return (
        <div className='cmc-widget'>
            <Helmet>
                <script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js" />
            </Helmet>
            <div className="coinmarketcap-currency-widget" data-currencyid={currencyID} data-base="EUR" data-ticker="false"
                 data-rank="false" data-marketcap="false" data-volume="false" data-statsticker="false" data-stats="EUR"/>
        </div>
    )
}

const Rotator = () => {
    const [state, dispatch] = useTracked();
    const prev = prevFaucet(state.faucetIdx);
    const next = nextFaucet(state.faucetIdx);
    return (
        <Toolbar>
            <div className="p-toolbar-group-left">
                <Button onClick={() => dispatch({type: 'flipNavigation'})} icon="fa fa-fw fa-bars" style={{marginRight:10}}/>
            </div>
            <div className="p-toolbar-group-right">
                <Button onClick={() => dispatch({type: 'restart'})} icon="fa fa-fw fa-undo-alt" style={{marginLeft:4}}/>
                <Button label={prev.label} onClick={() => dispatch({type: 'decrement'})}
                        icon="fa fa-fw fa-arrow-circle-left" style={{marginLeft:4, background:prev.color, borderColor:prev.color}} />
                <Button label={next.label} onClick={() => dispatch({type: 'increment'})}
                        icon="fa fa-fw fa-arrow-circle-right" iconPos="right" style={{marginLeft:4, background:next.color, borderColor:next.color}}/>
            </div>
        </Toolbar>
    )
}

const App = () => {
    return (
        <Provider reducer={reducer} initialState={initialState}>
            <Router>
            <div className="layout-wrapper">
                <div className="header">
                    <Rotator/>
                </div>
                <div>
                    <Routes/>
                </div>
            </div>
            </Router>
        </Provider>
    );
};

export default App;
