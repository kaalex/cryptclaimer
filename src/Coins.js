import React, { Component } from 'react';
import {Helmet} from "react-helmet";

export default class Coins extends Component {

    render() {
        return (
            <div className="coins-content" style={{textAlign:"left", padding:"2em"}}>
                <Helmet>
                    <script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js" />
                </Helmet>
                <div className="coinmarketcap-currency-widget" data-currencyid="1" data-base="EUR" data-secondary="" data-ticker="true"
    data-rank="true" data-marketcap="true" data-volume="true" data-statsticker="true" data-stats="EUR"/>
                <div className="coinmarketcap-currency-widget" data-currencyid="1831" data-base="EUR" data-secondary="" data-ticker="true"
    data-rank="true" data-marketcap="true" data-volume="true" data-statsticker="true" data-stats="EUR"/>

                <div className="coinmarketcap-currency-widget" data-currencyid="2" data-base="EUR" data-secondary="" data-ticker="true"
    data-rank="true" data-marketcap="true" data-volume="true" data-statsticker="true" data-stats="EUR"/>

                <div className="coinmarketcap-currency-widget" data-currencyid="131" data-base="EUR" data-secondary="" data-ticker="true"
    data-rank="true" data-marketcap="true" data-volume="true" data-statsticker="true" data-stats="EUR"/>

                <div className="coinmarketcap-currency-widget" data-currencyid="74" data-base="EUR" data-secondary="" data-ticker="true"
    data-rank="true" data-marketcap="true" data-volume="true" data-statsticker="true" data-stats="EUR"/>
            </div>
        )
    }
}